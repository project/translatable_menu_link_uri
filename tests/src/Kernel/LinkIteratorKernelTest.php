<?php

declare(strict_types=1);

namespace Drupal\Tests\translatable_menu_link_uri\Kernel;

use Drupal\KernelTests\KernelTestBase;

class LinkIteratorKernelTest extends KernelTestBase {
  protected static $modules = ['translatable_menu_link_uri'];

  public function setUp(): void {
    parent::setUp();
  }

  public function testServiceContainer() {
    $service = $this->container->get('translatable_menu_link_uri.link_processor');
    $this->assertNotNull($service);
  }


  public function testNoTranslationCase() {
  }
}
