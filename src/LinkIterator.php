<?php

namespace Drupal\translatable_menu_link_uri;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\link\LinkItemInterface;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Psr\Log\LoggerInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;

/**
 * Implementation of Link processor Service.
 *
 * @package Drupal\translatable_menu_link_uri
 */
class LinkIterator {

  /**
   * Service constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   */
  public function __construct(
    protected LoggerInterface $logger,
    protected EntityRepositoryInterface $entityRepository,
    protected ModuleHandlerInterface $moduleHandler,
    protected Token $token,
  ) {
  }

  /**
   * Iterates over the menu and checks for sublevel menu links.
   *
   * @param array &$item
   *   The menu item to process.
   */
  public function process(array &$item): void {
    try {
      $this->handleSubLevelMenu($item);

      /** @var \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent|null */
      $link = $item['original_link'] ?? NULL;

      if ($link instanceof MenuLinkContent) {
        $url = $this->getUrl($link);
      }

      if (!is_null($url)) {
        $options = $this->prepareUrlOptions($item, $link);
        $item = $this->updateLink($item, $url);
        $item = $this->updateUrlWithTokenReplacement($item, $link);
        $item = $this->updateOptions($options, $item, $link);
      }
    }
    catch (\Exception $e) {
      $this->logger->warning(
        $e->getMessage()
      );
    }
  }

  /**
   * Handles sublevel menu links.
   *
   * @param array &$item
   *   The menu item to process.
   */
  private function handleSubLevelMenu(array &$item): void {
    if (isset($item['below'])) {
      foreach (array_keys($item['below']) as $key) {
        $this->process($item['below'][$key]);
      }
    }
  }

  /**
   * Updates URL options for a menu link content item.
   *
   * @param array &$item
   *   The menu item to process.
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link content entity.
   *
   * @return array
   *   The processed options from URL related to menu item.
   */
  private function prepareUrlOptions(array &$item, MenuLinkContent $link): array {
    $options = $item['url']->getOptions();
    unset($options['query']);
    unset($options['fragment']);

    return $options;
  }

  /**
   * Updates URL options for a menu link content item.
   *
   * @param array &$options
   *   The retrieved options list.
   * @param array &$item
   *   The menu item to process.
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link content entity.
   *
   * @return array
   *   The menu item with updated options.
   */
  private function updateOptions(array &$options, array &$item, MenuLinkContent $link): array {
    if (!empty($options)) {
      // Inherit only options that have not been overridden.
      $overridden_options = $item['url']->getOptions();
      $options += $overridden_options;
      $item['url']->setOptions($options);
    }

    return $item;
  }

  /**
   * Updates link URL.
   *
   * @param array &$item
   *   The menu item to process.
   * @param \Drupal\link\LinkItemInterface|null $url
   *   The updated URL.
   *
   * @return array|null
   *   Update menu item with updated url.
   */
  private function updateLink(array &$item, ?LinkItemInterface $url): array {
    if ($url instanceof LinkItemInterface) {
      $item['url'] = $url->isEmpty() ? $item['url'] : $url->getUrl();
    }

    return $item;
  }

  /**
   * Get processed URL.
   *
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link content entity.
   *
   * @return \Drupal\link\LinkItemInterface|null
   *   The LinkItem.
   */
  private function getUrl(MenuLinkContent $link): ?LinkItemInterface {
    $reflection = new \ReflectionMethod(MenuLinkContent::class, 'getEntity');
    if (method_exists(MenuLinkContent::class, 'getEntity') && $reflection->isPublic()) {
      return $link->getEntity()->link_override?->first();
    }
    else {
      $uuid = $link->getDerivativeId();
      $entity = $this->entityRepository->loadEntityByUuid('menu_link_content', $uuid);
      $translation = !is_null($entity) ? $this->entityRepository->getTranslationFromContext($entity) : NULL;

      return is_null($translation) ? NULL : $translation->link_override->first();
    }
  }

  /**
   * Updates the URL with token replacements.
   *
   * @param array &$item
   *   The menu item to process.
   * @param \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link
   *   The menu link content entity.
   */
  private function updateUrlWithTokenReplacement(array &$item, MenuLinkContent $link): array {
    if ($item["url"]->isExternal() && $this->moduleHandler->moduleExists('token')) {
      $tokenized_uri = $this->token->replace($item["url"]->getUri());
      $item['url'] = Url::fromUri($tokenized_uri, []);
    }

    return $item;
  }

}
