<?php

namespace Drupal\translatable_menu_link_uri;


interface LinkIteratorInterface {
  /**
   * Iterates over the menu and checks for sublevel menu links.
   *
   * @param array &$item
   *   The menu item to process.
   */
  public function Process(array &$item): void;

}
